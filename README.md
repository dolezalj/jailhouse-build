Repository with Jailhouse build configuration for Jetson TX2

Building
========


    wget https://debian.pengutronix.de/debian/pool/main/o/oselas.toolchain/oselas.toolchain-2014.12.2-aarch64-v8a-linux-gnu-gcc-4.9.2-glibc-2.20-binutils-2.24-kernel-3.16-sanitized_2014.12.2_amd64.deb
    sudo dpkg -i oselas.toolchain-2014.12.2-aarch64-v8a-linux-gnu-gcc-4.9.2-glibc-2.20-binutils-2.24-kernel-3.16-sanitized_2014.12.2_amd64.deb
    git submodule update --init
    cd build
    make

The above cross-compiles the following: Linux kernel, Jailhouse and
simple root filesystem.

Building on TX2 (only Jailhouse and Linux)
------------------------------------------

    make jailhouse CROSS_COMPILE=

Booting
=======

Network boot
------------

We use novaboot tool (https://github.com/wentasah/novaboot) to boot
the resulting images over network. Use the `build/boot` script to boot
Jailhouse or `build/boot-linux` to boot plain Linux without starting
Jailhouse.

Flash/SD card boot
------------------

To boot Linux and Jailhouse without novaboot, create a Debian package
by running:

	cd build
    make deb

Copy the resulting .deb package to the board and install it with

    dpkg -i jailhouse_*.deb

After rebooting, choose "prem kernel and jailhouse" entry from the
extlinux boot menu. Once the system boots, use `systemctl` command to
start/stop jailhouse:

	systemctl {start|stop|status} jailhouse
