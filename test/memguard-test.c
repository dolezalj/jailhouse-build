#define _GNU_SOURCE         /* See feature_test_macros(7) */
#include <err.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <inttypes.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <pthread.h>
#include <stdlib.h>
#include <error.h>

#ifndef SYS_memguard
#define SYS_memguard 793
#endif

/* Return value:
 * 0-31  - Sum of memory events
 * 32-55 - Total time
 * 61    - Memory budget overrun
 * 62    - Timeout
 * 63    - Error */
#define MGRET_MEM_POS		0
#define MGRET_TIM_POS		32
#define MGRET_OVER_MEM_POS	61
#define MGRET_OVER_TIM_POS	62
#define MGRET_ERROR_POS		63

#define MGRET_TIM_MASK		(0x00FFFFFFul << MGRET_TIM_POS)
#define MGRET_MEM_MASK		(0xFFFFFFFFul << MGRET_MEM_POS)
#define MGRET_OVER_MEM_MASK	(1ul << MGRET_OVER_MEM_POS)
#define MGRET_OVER_TIM_MASK	(1ul << MGRET_OVER_TIM_POS)
#define MGRET_ERROR_MASK	(1ul << MGRET_ERROR_POS)

#define MAX_CORES		6

char memory[128*10204*1024];

static pthread_barrier_t barrier;

struct mg_ret {
	uint32_t time;
	uint32_t mem;
	bool time_ovf;
	bool mem_ovf;
};

static struct mg_ret mgret(uint64_t retval)
{
	struct mg_ret mgr = {};
	mgr.time = (retval & MGRET_TIM_MASK) >> MGRET_TIM_POS;
	mgr.mem = (retval & MGRET_MEM_MASK) >> MGRET_MEM_POS;
	mgr.time_ovf = (retval >> MGRET_OVER_TIM_POS) & 1;
	mgr.mem_ovf = (retval >> MGRET_OVER_MEM_POS) & 1;
	return mgr;
}

void compute_kernel(int time_us)
{
	struct timespec ts;
	uint64_t current_us, end_us;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	end_us = ts.tv_sec * 1000000 + ts.tv_nsec / 1000 + time_us;
	do {
		clock_gettime(CLOCK_MONOTONIC, &ts);
		current_us = ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
	} while (current_us < end_us);
}

void read_memory(long lines)
{
	uint64_t sum = 0;

	for (uint64_t i = 0; i < lines; i++) {
		sum += memory[(i * 64) % sizeof(memory)];
	}
	/* Do not optimize this function out */
	volatile uint64_t x = sum;
	(void)x;
}

void write_memory(long lines)
{
	uint64_t *ptr;
	for (uint64_t i = 0; i < lines; i++) {
		ptr = (void*)&memory[(i * 64) % sizeof(memory)];
		*ptr = i;
	}
}

void read_memory_rnd(long lines)
{
	static unsigned rnd;
	uint64_t sum = 0;

	for (long i = 0; i < lines; i++) {
		rnd = (rnd + 523) * 253573;
		sum += memory[rnd % sizeof(memory)];
	}
	/* Do not optimize this function out */
	volatile uint64_t x = sum;
	(void)x;
}

/* Memguard flags */
#define MGF_PERIODIC                                                           \
	(1 << 0) /* Chooses between periodic or one-shot budget replenishment */
#define MGF_MASK_INT                                                           \
	(1                                                                     \
	 << 1) /* Mask (disable) low priority interrupts until next memguard call */

long memguard(unsigned long timeout, unsigned long memory_budget,
	      unsigned long flags)
{
	return syscall(SYS_memguard, timeout, memory_budget, flags);
}

void wvtest_pass(bool cond, const char* file, int line, const char* str)
{
	printf("! %s:%d  %s %s\n", file, line, str, cond ? "ok" : "FAILURE");
}

#define WVPASS(cond) wvtest_pass(cond, __FILE__, __LINE__, #cond)

static void print_test_info(uint64_t timeout_us, uint64_t mem_budget, uint64_t flags,
			    int64_t retval, const char *code)
{
	char call[80];
	snprintf(call, sizeof(call), "memguard(%luus, %lumiss, %c%c):",
		 timeout_us, mem_budget,
		 flags & MGF_PERIODIC ? 'P' : '-',
		 flags & MGF_MASK_INT ? 'I' : '-'
		);
	struct mg_ret r = mgret(retval);
	printf("Testing \"CPU%d: %-40s %-25s ⇒ time:%8u%c mem:%8u%c\" in %s:\n",
	       sched_getcpu(), call, code,
	       r.time, r.time_ovf ? '!' : ' ',
	       r.mem, r.mem_ovf ? '!' : ' ',
	       __FILE__);
	WVPASS((retval & MGRET_ERROR_MASK) == 0);
}

pthread_mutex_t wvtest_lock = PTHREAD_MUTEX_INITIALIZER;

#define MGTEST(timeout_us, mem_budget, flags, code) 			\
	({								\
		long retval;						\
		retval = memguard(timeout_us, mem_budget, flags);	\
		code;							\
		retval = memguard(0, 0, 0);				\
		pthread_mutex_lock(&wvtest_lock);			\
		print_test_info(timeout_us, mem_budget, flags, retval, #code); \
		mgret(retval);						\
	})

#define MGTESTEND() pthread_mutex_unlock(&wvtest_lock)

void *test_thread(void *ptr)
{
	cpu_set_t set;
	int cpu = (intptr_t)ptr;

	/* Ensure that our test thread does not migrate to another CPU
	 * during memguarding */
	CPU_ZERO(&set);
	CPU_SET(cpu, &set);
	if (sched_setaffinity(0, sizeof(set), &set) < 0)
		err(1, "sched_setaffinity");

	pthread_barrier_wait(&barrier);

   	struct mg_ret r;

	for (uint64_t flags = 0; flags < 4; flags++) {
		compute_kernel(1); /* warm up */

		///////////////////////////////////////////////////////
		r = MGTEST(5000, 10000, flags, compute_kernel(1000));
		WVPASS(!r.time_ovf);
		WVPASS(r.time > 900);
		MGTESTEND();

		r = MGTEST(500, 10000, flags, compute_kernel(1000));
		WVPASS(r.time_ovf);
		WVPASS(r.time > 900);
		MGTESTEND();

		r = MGTEST(5000, 10000, flags, compute_kernel(2000));
		WVPASS(!r.time_ovf);
		WVPASS(r.time > 1900);
		MGTESTEND();

		r = MGTEST(500, 10000, flags, compute_kernel(2000));
		WVPASS(r.time_ovf);
		WVPASS(r.time > 1900);
		MGTESTEND();

		///////////////////////////////////////////////////////
		r = MGTEST(100000, 500000, flags, read_memory(100000));
		WVPASS(!r.mem_ovf);
		WVPASS(r.mem >= 90000);
		MGTESTEND();

		r = MGTEST(100000, 50000,  flags, read_memory(100000));
		WVPASS(r.mem_ovf);
		WVPASS(r.mem >= 90000);
		MGTESTEND();

		r = MGTEST(100000, 500000, flags, read_memory_rnd(100000));
		WVPASS(!r.mem_ovf);
		WVPASS(r.mem >= 90000);
		MGTESTEND();

		r = MGTEST(100000, 50000,  flags, read_memory_rnd(100000));
		WVPASS(r.mem_ovf);
		WVPASS(r.mem >= 90000);
		MGTESTEND();

		r = MGTEST(100000, 500000, flags, write_memory(100000));
		WVPASS(!r.mem_ovf);
		WVPASS(r.mem >= 90000);
		MGTESTEND();

		r = MGTEST(100000, 50000,  flags, write_memory(100000));
		WVPASS(r.mem_ovf);
		WVPASS(r.mem >= 90000);
		MGTESTEND();


		///////////////////////////////////////////////////////
		r = MGTEST(100000, 5000000, flags, read_memory(1000000));
		WVPASS(!r.mem_ovf);
		WVPASS(r.mem >= 900000);
		MGTESTEND();

		r = MGTEST(100000, 500000,  flags, read_memory(1000000));
		WVPASS(r.mem_ovf);
		WVPASS(r.mem >= 900000);
		MGTESTEND();

		r = MGTEST(100000, 5000000, flags, read_memory_rnd(1000000));
		WVPASS(!r.mem_ovf);
		WVPASS(r.mem >= 900000);
		MGTESTEND();

		r = MGTEST(100000, 500000,  flags, read_memory_rnd(1000000));
		WVPASS(r.mem_ovf);
		WVPASS(r.mem >= 900000);
		MGTESTEND();

		r = MGTEST(100000, 5000000, flags, write_memory(1000000));
		WVPASS(!r.mem_ovf);
		WVPASS(r.mem >= 900000);
		MGTESTEND();

		r = MGTEST(100000, 500000,  flags, write_memory(1000000));
		WVPASS(r.mem_ovf);
		WVPASS(r.mem >= 900000);
		MGTESTEND();

		printf("\n");
	}

	/* Throttling tests */
	struct mg_ret r1, r2, r3;
	r1 = MGTEST(10000, 9000, MGF_PERIODIC | MGF_MASK_INT, read_memory(100000));
	WVPASS(r1.time > 100*1000);
	MGTESTEND();

	r2 = MGTEST(10000, 3000, MGF_PERIODIC | MGF_MASK_INT, read_memory(100000));
	WVPASS(r2.time > 2 * r1.time);
	MGTESTEND();

	r3 = MGTEST(10000, 1000, MGF_PERIODIC | MGF_MASK_INT, read_memory(100000));
	WVPASS(r3.time > 2 * r2.time);
	MGTESTEND();

	return NULL;
}

int main(int argc, char *argv[])
{
	int cpu_mask = 1;
	int cpu_count = 0;
	pthread_t threads[MAX_CORES];

	/* TODO: currently shared memory */
	for (int i = 0; i < sizeof(memory); i += 64)
		memory[i] = 1;

	if (argc > 1)
		cpu_mask = strtol(argv[1], NULL, 16);

	for (int i = 0; i < MAX_CORES; i++) {
		if (cpu_mask & (1 << i)) {
			cpu_count++;
		}
	}

	printf("CPU count:%d  CPU mask:%#x\n", cpu_count, cpu_mask);
	int s = pthread_barrier_init(&barrier, NULL, cpu_count);
	if (s != 0)
		error(1, s, "pthread_barrier_init");

	for (intptr_t i = 0; i < MAX_CORES; i++) {
		if (cpu_mask & (1 << i))
			pthread_create(&threads[i], NULL, test_thread, (void *)i);
	}

	for (int i = 0; i < MAX_CORES; i++) {
		if (cpu_mask & (1 << i)) {
			pthread_join(threads[i], NULL);
		}
	}

	return 0;
}
